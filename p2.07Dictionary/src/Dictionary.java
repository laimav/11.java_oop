import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Dictionary {
	//private String word;
	//private String translation;
	private HashMap<String, String> dictionary = new HashMap<String, String>();
	
//  Returns the translation of its parameter.  If he word is unknown, it returns null.
	public String translate(String word) {
		// this.word = word;
		if (dictionary.containsKey(word)) {
			return dictionary.get(word);
		}
		else {
			return null;
		}	
	}
	
	// Adds a new translation to the dictionary
	public void add(String word, String translation) {
		dictionary.put(word, translation);
	}
	
	// returns the amount of words in the dictionary.
	public int amountOfWords() {
		return dictionary.size();
	}
	
	//returning strings which stand for a content list of your dictionary in the form key = value.
	public ArrayList<String> translationList() {
		ArrayList<String> translationList = new ArrayList<String>();
		for(Map.Entry<String, String> entry: dictionary.entrySet()) {
			translationList.add( entry.getKey() + " = " + entry.getValue() );
		}
		return translationList;
	}
	
	
}
