import java.util.Scanner;


public class TextUserInterface {
	private Scanner reader = new Scanner(System.in);
	private Dictionary dictionary = new Dictionary();

	public TextUserInterface(Scanner reader, Dictionary dictionary) {
		this.reader = reader;
		this.dictionary = dictionary;
	}

	public void start() {
		System.out.println("Statements:");
		System.out.println("  add - adds a word pair to the dictionary");
		System.out.println("  translate - asks a word and prints its translation");
		System.out.println("  quit - quits the text user interface");

		/*		System.out.println("\nStatement: ");
		String entry = reader.nextLine();

		while(entry != "quit") {
			//			System.out.println("Statement: ");
			//			entry = reader.nextLine();

			if (entry.equals("add"))  {
				System.out.print("In Finnish: ");
				String word1 = reader.nextLine();
				System.out.print("Translation: ");
				String translation = reader.nextLine();
				add(word1, translation);
				System.out.print("\nStatement: ");
				entry = reader.nextLine();
			}

			if (entry.equals("translate")) {
				System.out.print("Give a word: ");
				String word2 = reader.nextLine();
				System.out.print("Translation: ");
				translate(word2);
				System.out.print("\nStatement: ");
				entry = reader.nextLine();
			}

			if (entry.equals("quit"))  {
				System.out.print("Cheers!");	
				break;
			}

			else {
				System.out.println("Unknown statement");
				System.out.println("\nStatement: ");
				entry = reader.nextLine();
			}
		}*/


		System.out.println("\nStatement: ");
		String entry = reader.nextLine();

		while(!entry.equals("quit")) {
			
			switch (entry) {
			case "add":
				System.out.print("In Finnish: ");
				String word1 = reader.nextLine();
				System.out.print("Translation: ");
				String translation = reader.nextLine();
				add(word1, translation);
				System.out.println("\nStatement: ");
				entry = reader.nextLine();
				break;
			case "translate":
				System.out.print("Give a word: ");
				String word2 = reader.nextLine();
				System.out.print("Translation: ");
				translate(word2);
				System.out.println("\nStatement: ");
				entry = reader.nextLine();
				break;
			case "quit":
				System.out.print("Cheers!");	
				break;
			default:
				System.out.println("Unknown statement");
				System.out.println("\nStatement: ");
				entry = reader.nextLine();
				break;
			}
			//break;
		
		}


	}

	// The command add asks for a word pair from the user and adds them to the dictionary. 
	public void add(String word, String translation) {
		/*		String entry = reader.nextLine();
		if (entry.equals("add"))  {*/
		dictionary.add(word, translation);
		//}
	}

	// The command translate asks a word from the user and it prints the translation.
	public void translate(String word) {
		/*		String entry = reader.nextLine();
		if (entry.equals("translate"))  {*/
		System.out.println(dictionary.translate(word));
		//}
	}


}

