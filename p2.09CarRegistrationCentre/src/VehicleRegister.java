import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

public class VehicleRegister {

	private HashMap<RegistrationPlate, String> owners = new HashMap<RegistrationPlate, String>();

	public VehicleRegister() {
	}

	// adds the parameter owner of the car which corresponds to the parameter
	// registration plate. The method returns true if the car had no owner; if the
	// car had an owner already, the method returns false and it doesn't do anything
	public boolean add(RegistrationPlate plate, String owner) {
		if (owners.containsKey(plate)) { // jei PLATE turi OWNER -> true
			return false;
		} else {
			owners.put(plate, owner);
			return true;
		}
	}

	// returns the car owner which corresponds to the parameter register number.
	// If the car was not registered, it returns null.
	public String get(RegistrationPlate plate) {
		return owners.get(plate);
	}

	// delete the information connected to the parameter registration plate. The
	// method returns true if the information was deleted, and false if there was no
	// information connected to the parameter in the register.
	public boolean delete(RegistrationPlate plate) {
		if (owners.containsKey(plate)) {
			owners.remove(plate);
			return true;
		}
		return false;
	}

	public void printRegistrationPlates() {
		// which prints out all the registration plates stored
		for (RegistrationPlate pair : owners.keySet()) {
			System.out.println(pair);
		}
	}

	public void printOwners() {
		//which prints all the car owners stored. Each owner's name has to be printed only once, even though they had more than one car.
		List<String> ownersList = new ArrayList<String>();
		for (String owner : owners.values()) {
			if (!ownersList.contains(owner)) {
				ownersList.add(owner);
			}
		}
		for(String o : ownersList) {
			System.out.println(o);
		}
		
	}

}
