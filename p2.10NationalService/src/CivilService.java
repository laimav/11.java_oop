
public class CivilService implements NationalService{
	int daysLeft = 362;

	public CivilService() {
	}
	
	@Override
	public int getDaysLeft() {
		return daysLeft;
	}
	
	@Override
	public void work() {
		if(daysLeft>0) {
			daysLeft--;
		}		
		else { 
			System.out.println("Work is over!");
		}
	}

}
