public class Main {
    public static void main(String[] args) {
    	CivilService civil = new CivilService();
    	System.out.println("Days left: " + civil.getDaysLeft());
    	civil.work();
    	System.out.println("Days left: " + civil.getDaysLeft());
    	civil.work();
    	civil.work();
    	civil.work();
    	System.out.println("Days left: " + civil.getDaysLeft());

    	System.out.println("Military service:");
    	MilitaryService military = new MilitaryService(5);
    	System.out.println("Days left: " + military.getDaysLeft());
    	military.work();
    	military.work();
    	military.work();
    	military.work();
    	military.work();
    	military.work();

    	System.out.println("Days left: " + military.getDaysLeft());

    
    
    }
}
