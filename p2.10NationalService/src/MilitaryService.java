
public class MilitaryService implements NationalService{
	public int daysLeft;

	public MilitaryService(int daysLeft) {
		this.daysLeft = daysLeft;
	}

	@Override
	public int getDaysLeft() {
		return daysLeft;
	}

	@Override
	public void work() {
		if(daysLeft>0) {
			daysLeft--;
		}		
		else { 
			System.out.println("Work is over!");
		}
	}

}
