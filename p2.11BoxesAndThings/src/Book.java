
public class Book implements ToBeStored{
	private String writer;
	private String name;
	private double weight;
	
	public Book(String writer, String name, double weight) {
		// Book receives its writer (String), name (String), and weight (double), all as parameter.
		this.writer = writer;
		this.name = name;
		this.weight = weight;
	}
	
	@Override
	public double weight() {
		return weight;
	}

	@Override
	public String toString() {
		return writer + ": " + name;
	}

}
