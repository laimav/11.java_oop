import java.util.ArrayList;
import java.util.List;

public class Box implements ToBeStored{
	private double weight;
	private double maxWeight;
	private List<ToBeStored> boxList = new ArrayList<ToBeStored>();

	public Box(double maxWeight) {
		this.maxWeight = maxWeight;

	}

	@Override
	public double weight() {
		double weight = 0;
		for(ToBeStored o : boxList){
			weight+=o.weight();
		}
		return weight;
	}

	public void add(ToBeStored o) {
/*				if (o instanceof Book || o instanceof CD) {
					if ( ((Box) o).weight()<= maxWeight ) {
				}
					boxList.add(o);
					maxWeight -=  ((Box) o).weight();
				}
			}*/
		
		if (o.weight() + this.weight() <= maxWeight) {
			boxList.add(o);
		}
	}
	
	@Override
    public String toString() {
        return "Box: " + boxList.size() + " things, total weight " + this.weight() + " kg";
    }
	
}

