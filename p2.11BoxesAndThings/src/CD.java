
public class CD implements ToBeStored{
	private String artist;
	private String title;
	private int year;
	public final double weight = 0.1;
	
	
	public CD(String artist, String title, int year) {
		// CD's parameter contains its artist (String), title (String), and publishing year (int). All CDs weight 0.1 kg.
		this.artist=artist;
		this.title=title;
		this.year=year;
	}
	
    @Override
	public double weight() {
		return weight; 			//return 0.1;
	}
	
	@Override
	public String toString() {
		return artist + ": " + title + " (" + year + ")";
	}

}
