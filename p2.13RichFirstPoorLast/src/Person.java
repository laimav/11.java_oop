public class Person implements Comparable<Person>{

	private int salary;
	private String name;

	public Person(String name, int salary) {
		this.name = name;
		this.salary = salary;
	}

	public String getName() {
		return name;
	}

	public int getSalary() {
		return salary;
	}


	@Override
	public String toString() {
		return name + " " + salary;
	}

	//ompareTo method would sort the people according to their salary -- rich first, poor last. 
	@Override
	public int compareTo(Person o) {
		/*if (this == o || this.salary == o.salary) {
			return 0;
		} 
		else if (this.salary < o.salary) { 
			return 1;
		}
		else if (this.salary > o.salary) { 
			return -1;
		}
		else {
			throw new RuntimeException("Nesusipratimas :(");
		}*/
		
		return (o.getSalary() - this.getSalary());

	}




}
