public class Card implements Comparable<Card> {

	/*
	 * These are static constant variables. These variables can be used inside and
	 * outside of this class like, for example, Card.CLUBS
	 */
	public static final int SPADES = 0;
	public static final int DIAMONDS = 1;
	public static final int HEARTS = 2;
	public static final int CLUBS = 3;
	/*
	 * To make printing easier, Card-class also has string arrays for suits and
	 * values. SUITS[suit] is a string representation of the suit (Clubs, Diamonds,
	 * Hearts, Spades) VALUES[value] is an abbreviation of the card's value (A, J,
	 * Q, K, [2..10]).
	 */
	public static final String[] SUITS = { "Spades", "Diamonds", "Hearts", "Clubs" };
	public static final String[] VALUES = { "-", "-", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K",
			"A" };
	private int value;
	private int suit;

	public Card(int value, int suit) {
		this.value = value;
		this.suit = suit;
	}

	@Override
	public String toString() {
		return VALUES[value] + " of " + SUITS[suit];
	}

	public int getValue() {
		return value;
	}

	public int getSuit() {
		return suit;
	}

	/*
	 * cards should be sorted in ascending order according to their value. If the
	 * value of two classes have got the same values, we compare them against their
	 * suit in ascending order: spades first, diamonds second, hearts third, and
	 * clubs last.
	 */

	@Override
	public int compareTo(Card o) {
		int valueCriteria = this.getValue() - o.getValue();
		int suitCriteria = this.getSuit() - o.getSuit();

		if (valueCriteria == 0) {
			return suitCriteria;
		}
		return valueCriteria;

		/*
		 * if (this == o || this.getValue() == o.getValue()) { return 0; } else if
		 * (this.getValue() < o.getValue()) { return -1; } else if (this.getValue() >
		 * o.getValue()) { return 1; } else { throw new
		 * RuntimeException("Nesusipratimas :("); }
		 */

	}

}
