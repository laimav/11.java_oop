import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Hand implements Comparable<Hand> {
	private List<Card> handSet = new ArrayList<Card>();

	public Hand() {
	}

	public void add(Card card) {
		// adds a card to the hand
		handSet.add(card);
	}

	public void print() {
		// prints the cards in the hand following the below example pattern
		for (int i = 0; i < handSet.size(); i++) {
			System.out.println(handSet.get(i).toString());
		}
	}

	// Arrays.sort(handSet, (s1, s2) -> s1.length() - s2.length());

	// Sorts the cards in the hand. Sorted cards are printed in order:
	public void sort() {
		// List<Card> handSet = new ArrayList<Card>(new HandSetComparator());

		Collections.sort(handSet, new HandSetComparator());
		// arba kitas variantas: handSet.sort(..);
	}

	@Override
	public int compareTo(Hand handX) {
		int sum1 = 0;
		int sum2 = 0;
		for (int i = 0; i < this.handSet.size(); i++) {
			sum1 += this.handSet.get(i).getValue();
			// sum1 += this.handSet.get(i).getSuit();
		}
		for (int i = 0; i < handX.handSet.size(); i++) {
			sum2 += handX.handSet.get(i).getValue();
			// sum2 += handX.handSet.get(i).getSuit();
		}

		return sum1 - sum2;

	}

	public void sortAgainstSuit() {
		Collections.sort(handSet, new SortAgainstSuitAndValue());
	}

}
