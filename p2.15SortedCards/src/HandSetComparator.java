import java.util.Comparator;

public class HandSetComparator implements Comparator<Card> {

	@Override
	public int compare(Card o1, Card o2) {
		int valueCriteria = o1.getValue() - o2.getValue();
		int suitCriteria = o1.getSuit() - o2.getSuit();

		if (valueCriteria == 0) {
			return suitCriteria;
		}
		return valueCriteria;
	}

}
