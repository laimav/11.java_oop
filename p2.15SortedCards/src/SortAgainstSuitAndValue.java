import java.util.Comparator;

public class SortAgainstSuitAndValue implements Comparator<Card> {

	public int compare(Card card1, Card card2) {
		int suitCriteria = card1.getSuit() - card2.getSuit();
		int valueCriteria = card1.getValue() - card2.getValue();

		if (suitCriteria == 0) {
			return valueCriteria;
		}
		return suitCriteria;
	}
}