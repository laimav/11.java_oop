package people;

public class Person {
	private String name;
	private String address;

	public Person() {
	}

	public Person(String vardas, String adresas) {
		this.name = vardas;
		this.address = adresas;
	}
	
	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}


	@Override
	public String toString() {
		return name + "\n  " + address;
	}
	
	
	

}
