package people;

public class Student extends Person{
	private int credit = 0;

	public Student() {
	}
	
	public Student(String vardas, String adresas) {
		super(vardas, adresas);
	}
	
	public int getCredit() {
		return credit;
	}


	public int credits() {
		//System.out.println("credits " + credit);
		return credit; 
	}

	public void study() {
		credit++;
	}

	@Override
	public String toString() {
		return super.toString() + "\n  credits " + credit;
	}
	
	
}
