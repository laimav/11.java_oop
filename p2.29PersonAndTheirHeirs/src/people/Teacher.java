package people;

public class Teacher extends Person{
	private int salary;

	public Teacher() {
	}

	public Teacher(String vardas, String adresas, int salary) {
		super(vardas, adresas);
		this.salary=salary;
	}

	@Override
	public String toString() {
		return super.toString() + "\n  salary " + salary + " euros/month";
	}


}
