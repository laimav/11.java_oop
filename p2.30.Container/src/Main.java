import containers.ProductContainer;
import containers.ProductContainerRecorder;

public class Main {

	public static void main(String[] args) {
		/*	ProductContainer juice = new ProductContainer("Juice", 1000.0); 
		juice.addToTheContainer(1000.0); 
		juice.takeFromTheContainer(11.3); 
		System.out.println(juice.getName()); 
		System.out.println(juice);    
		}

		 */

		/*		ProductContainer juice = new ProductContainer("Juice", 1000.0); 
		juice.addToTheContainer(1000.0); 
		juice.takeFromTheContainer(11.3); 
		System.out.println(juice.getName());
		juice.addToTheContainer(1.0); 
		System.out.println(juice);*/


		ProductContainerRecorder juice = new ProductContainerRecorder("Juice", 1000.0, 1000.0);
		System.out.println(juice);
		juice.takeFromTheContainer(11.3);
		System.out.println(juice);
		System.out.println(juice.getName()); // Juice
		juice.addToTheContainer(1.0);
		System.out.println(juice); // Juice: volume = 989.7, free space 10.3

	}

}
