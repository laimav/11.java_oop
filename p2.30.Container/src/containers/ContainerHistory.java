package containers;

import java.util.ArrayList;
import java.util.Iterator;

public class ContainerHistory {

	private double situation;
	private ArrayList<Double> history = new ArrayList<Double>();

	public ContainerHistory() {
	}


	public double getSituation() {
		return situation;
	}


	public void setSituation(double situation) {
		this.situation = situation;
	}


	public void add(double situation) {
		// 	adds the parameter situation to the end of the container history.	
		this.situation = situation;
		history.add(situation);
	}

	public void reset() {
		// it deletes the container history records.
		history.clear();
	}


	public String toString() {
		// returns the container history in the form of a String. 
		// The String form given by the ArrayList class is fine and doesn't have to be modified.
		return history.toString();
	}

	public double maxValue() {
		//returns the greatest value in the container history. If the history is empty, the method returns 0.
		double max = 0;
		if (history.isEmpty()) {
			return max;
		}
		for (Double h1 : history) {
			if(h1>max) {
				max = h1;
			}
		}
		return max;
	}


	public double minValue() {
		//returns the smallest value in the container history. If the history is empty, the method returns 0.
		double min = 0;
		if (history.isEmpty()) {
			return min;
		}
		for (Double h1 : history) {
			if(h1<min) {
				min = h1;
			}
		}
		return min;
	}

	public double average() {
		//returns the average of the values in the container history. If the history is empty, the method returns 0.
		double avg = 0;
		int count = 0;
		double sum = 0;
		if (history.isEmpty()) {
			return avg;
		}
		for (Double h1 : history) {
			sum += h1;
			count++;
		}
		avg = sum/count;
		return avg;
	}


	public double greatestFluctuation() {
		//returns the absolute value of the single greatest fluctuation in the container history
		double maxFluctuation = 0;
		if( history.size()<=1 ) {
			return maxFluctuation;
		}
		for (Double h1 : history) {
			if (Math.abs(h1)>maxFluctuation) {
				maxFluctuation = h1;
			}
		}
		return maxFluctuation;
	}


	public double variance() {
	//	returns the sample variance of the container history values. 
	double variance = 0;
	if( history.size()<=1 ) {
		return variance;
	}
	for(Double h1 : history) {
		variance += Math.pow((h1 - this.average()), 2);
    }
	return variance / (history.size()-1);
	}


}
