package containers;

public class ProductContainerRecorder extends ProductContainer{
	
	//creates a product container. The product name, capacity, and original volume are given as parameter.
	// Record the original volume both as the stored product original volume and as the first value of the container history.
	
	ContainerHistory history = new ContainerHistory();
	
	public ProductContainerRecorder(String productName, double capacity, double initialVolume) {
		
		super(productName, capacity);
		initialVolume = getVolume();
		this.history.add(initialVolume);
	}

	public String history() {
	//returns the container history in the following form: [0.0, 119.2, 21.2]. Use the String printout form as it is.
		return history.toString();
	}
	                                                                              
}
